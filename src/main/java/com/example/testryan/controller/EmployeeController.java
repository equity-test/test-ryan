package com.example.testryan.controller;

import com.example.testryan.mapper.EmployeeMapper;
import com.example.testryan.model.Employee;
import com.example.testryan.model.response.StdResponse;
import com.example.testryan.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("api_fe")
public class EmployeeController {
    @Autowired
    EmployeeRepository employeeRepository;

    @GetMapping("/list_employee")
    public ResponseEntity<StdResponse<List<Employee>>> getEmployeeHierarchyById(@RequestParam Integer employeeId){
        Map<String, Object> mapEmployee = employeeRepository.getEmployeeHierarchyById(employeeId);
        List<Employee> lEmployee = EmployeeMapper.mapEmployees(mapEmployee, 1);

        StdResponse<List<Employee>> response = new StdResponse<List<Employee>>(HttpStatus.OK.toString(), "", lEmployee);
        return new ResponseEntity<StdResponse<List<Employee>>>(response, HttpStatus.OK);
    }
}
