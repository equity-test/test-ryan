package com.example.testryan.controller;

import com.example.testryan.helper.CSVHelper;
import com.example.testryan.model.Employee;
import com.example.testryan.model.response.StdResponse;
import com.example.testryan.service.TransaksiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("api_fe")
public class TransaksiController {
    @Autowired
    TransaksiService transaksiService;

    @PostMapping("/postdata")
    public ResponseEntity<StdResponse<String>> uploadCSVTransaksi(@RequestParam("file") MultipartFile file){
        if (CSVHelper.hasCSVFormat(file)) {
            String fileName = file.getOriginalFilename();
            try {
                transaksiService.save(file);
            }catch (Exception e){
                StdResponse<String> response = new StdResponse<String>(HttpStatus.EXPECTATION_FAILED.toString(), "Could not upload the file", fileName);
                return new ResponseEntity<StdResponse<String>>(response, HttpStatus.EXPECTATION_FAILED);
            }

            StdResponse<String> response = new StdResponse<String>(HttpStatus.OK.toString(), "", fileName);
            return new ResponseEntity<StdResponse<String>>(response, HttpStatus.OK);
        }
        StdResponse<String> response = new StdResponse<String>(HttpStatus.BAD_REQUEST.toString(), "Format file invalid", "");
        return new ResponseEntity<StdResponse<String>>(response, HttpStatus.BAD_REQUEST);
    }
}
