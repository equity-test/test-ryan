package com.example.testryan.repository;

import com.example.testryan.model.Employee;

import java.util.List;
import java.util.Map;

public interface IEmployeeRepository {
    /*
    List<Employee> findAll();
     */

    public Map<String, Object> getEmployeeHierarchyById(Integer employeeId);
}
