package com.example.testryan.repository;

import com.example.testryan.model.entity.Transaksi;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ITransaksiRepository extends JpaRepository<Transaksi,Integer> {
}
