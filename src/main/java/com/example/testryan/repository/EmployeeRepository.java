package com.example.testryan.repository;

import com.example.testryan.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.CallableStatementCreator;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.stereotype.Repository;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
public class EmployeeRepository implements IEmployeeRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /*
    @Override
    public List<Employee> findAll() {
        return jdbcTemplate.query(
                "select * from tbl_employee",
                (rs, rowNum) -> new Employee(
                        rs.getInt("employee_id"),
                        rs.getString("employee_name"),
                        "", 0, "", ""
                )
        );
    }
    */

    public Map<String, Object> getEmployeeHierarchyById(Integer employeeId){
        List<SqlParameter> parameters = Arrays.asList(new SqlParameter(Types.INTEGER));
        return jdbcTemplate.call(new CallableStatementCreator() {
            @Override
            public CallableStatement createCallableStatement(Connection con) throws SQLException {
                CallableStatement cs = con.prepareCall("{call SP_EMPLOYEE_HIERARCHY(?)}");
                cs.setInt(1, employeeId);
                return cs;
            }
        }, parameters);
    }
}