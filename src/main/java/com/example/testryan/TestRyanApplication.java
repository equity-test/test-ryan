package com.example.testryan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestRyanApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestRyanApplication.class, args);
	}

}
