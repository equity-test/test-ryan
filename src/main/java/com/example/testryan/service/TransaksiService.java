package com.example.testryan.service;

import com.example.testryan.helper.CSVHelper;
import com.example.testryan.model.entity.Transaksi;
import com.example.testryan.repository.ITransaksiRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
public class TransaksiService {
    @Autowired
    ITransaksiRepository transaksiRepository;

    public void save(MultipartFile file){
        try {
            List<Transaksi> transaksiList = CSVHelper.csvToListTransaksi(file.getInputStream());
            transaksiRepository.saveAll(transaksiList);
        }catch (IOException e){
            throw new RuntimeException("Fail to store csv data: " + e.getMessage());
        }
    }
}
