package com.example.testryan.helper;

import com.example.testryan.model.entity.Transaksi;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class CSVHelper {
    public static String TYPE = "text/csv";
    static String[] HEADERs = { "Employee_id", "Amount", "Tgl_transaksi" };

    public static boolean hasCSVFormat(MultipartFile file) {

        if (!TYPE.equals(file.getContentType())) {
            return false;
        }

        return true;
    }

    public static List<Transaksi> csvToListTransaksi(InputStream is) {
        try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
             CSVParser csvParser = new CSVParser(fileReader, CSVFormat.DEFAULT
                     .withHeader(HEADERs)
                     .withFirstRecordAsHeader()
                     .withDelimiter(';')
                     .withIgnoreHeaderCase()
                     .withTrim());) {

            List<Transaksi> transaksiList = new ArrayList<Transaksi>();

            Iterable<CSVRecord> csvRecords = csvParser.getRecords();

            for (CSVRecord csvRecord : csvRecords) {
                Transaksi transaksi = new Transaksi();
                transaksi.setEmployeeId(Integer.parseInt(csvRecord.get("Employee_id")));
                transaksi.setAmount(BigDecimal.valueOf(Long.parseLong(csvRecord.get("Amount"))));
                transaksi.setTrxDate(ConvertDateHelper.convertStrDateHHMMSS(csvRecord.get("Tgl_transaksi")));
                transaksi.setCreateBy(1);
                transaksi.setCreateDate(LocalDateTime.now());
                transaksi.setUpdateBy(1);
                transaksi.setUpdateDate(LocalDateTime.now());

                transaksiList.add(transaksi);
            }

            return transaksiList;
        } catch (IOException e) {
            throw new RuntimeException("fail to parse CSV file: " + e.getMessage());
        }
    }
}
